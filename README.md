# LuaNotebooks

### Tutorials, simple challenges & explorations  

Collections of notebooks made thanks to: https://github.com/guysv/ilua.  
NB: ilua solution is  not 100% stable (some random bugs from time to time). Still very useful.  
Main purpose: Learning lua & keeping bits of codes that might be useful later.
